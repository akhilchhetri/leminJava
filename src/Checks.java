public class Checks {
    public static void		checkValid()
    {
        Room    tmp;
        int		s;
        int		e;

        s = 0;
        e = 0;
        tmp = Main.head;
        while (tmp != null)
        {
            if (tmp.type == 1)
            {
                if (tmp.links.length() == 0)
                    Main.Error("Map error!\nStart isn't linked up!");
                s++;
            }
            if (tmp.type == 2)
            {
                if (tmp.links.length() == 0)
                    Main.Error("Map error!\nEnd isn't linked up!");
                e++;
            }
            tmp = tmp.next;
        }
        if (s != 1)
            Main.Error("Map doesn't have a start properly defined!");
        if (e != 1)
            Main.Error("Map doesn't have a end properly defined!");
    }

    public static int       isEndStart(String id, Room r)
    {
        Room tmp;

        tmp = Main.head;
        while (tmp != null)
        {
            if (id.contentEquals(tmp.id))
                if (tmp.type == 2)
                    return (1);
            if (r.type == 1)
                return (1);
            tmp = tmp.next;
        }
        return (0);
    }

    public static int			populated()
    {
        Room tmp;

        tmp = Main.head;
        while (tmp != null)
        {
            if (tmp.pop > 0 && tmp.type != 2) {
                //System.out.println(tmp.type + " is still pop!");
                return (1);
            }
            tmp = tmp.next;
        }
        return (0);
    }
}
