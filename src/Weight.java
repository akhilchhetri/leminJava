public class Weight {
    public static void		setStartWeight()
    {
        Room    h;

        h = Main.head;
        while (h != null)
        {
            if (h.type == 1)
                if (h.weight == -1)
                    Main.Error("Start or End not linked up properly!");
            h = h.next;
        }
    }

    public static void		setWeight(Room r, int w)
    {
        String  slt[];
        int		n;
        int		b;

        if (r.weight != -1 && r.weight <= w)
            return ;
        r.weight = w;
        if (r.type != 2 && r.type != 1)
        Set.SetMapIndexStr(r.id, 5);
        slt = r.links.split(" ");
        n = slt.length;
        r = Main.head;
        while (r != null)
        {
            b = n + 1;
            while (--b > 0)
                if (slt[b - 1].contentEquals(r.id))
                    setWeight(r, w + 1);
            r = r.next;
        }
    }

    public static void		calcWeight()
    {
        Room r;

        r = Main.head;
        while (r != null)
        {
            if (r.type == 2)
                setWeight(r, 0);
            r = r.next;
        }
        setStartWeight();
    }
}
