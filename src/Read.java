import java.io.*;

public class Read {
	public static String line = null;
	public static BufferedReader bufferedReader;
    static String fileName;
    static FileReader fileReader;

	public static void     openFile( String file ){
		File pfile = new File(file);
		if (!pfile.exists() || pfile.isDirectory())
			Main.Error("File specified is invalid!");
		try {
			fileName = file;
            fileReader = new FileReader(fileName);
            bufferedReader = new BufferedReader(fileReader);
		}
		catch(FileNotFoundException ex) {
			Main.Error("Unable to open file '" + fileName + "'");
		}
	}

	public static String readLn() {
		try {
			while ((line = bufferedReader.readLine()) != null) {
				return (line);
			}
		}
		catch (IOException ex) {
			Main.Error("Error reading file '" + fileName + "'");
		}
		return (null);
	}

	public static void closeFile() {
        try {
            bufferedReader.close();
        }
        catch (IOException ex) {
            Main.Error("Error closing file '" + fileName + "'");
        }
    }
}
