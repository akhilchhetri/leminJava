public class Store {

    public static void		readNewNode(String str)
    {
        String[] slt;
        int x;
        int y;

        slt = str.split(" ");
        x = Integer.parseInt(slt[1]);
        y = Integer.parseInt(slt[2]);
        if (Main.Map[y][x] == 2)
            Node.addNode(str, 1);
        if (Main.Map[y][x] == 3)
            Node.addNode(str, 2);
        if (Main.Map[y][x] == 1)
            Node.addNode(str, 0);
        if (Main.Map[y][x] == 0)
            System.out.println("Tried to store a wall!");
    }

    public static void   readLinks( String str ) {
        String[]	slt;
        String      t;
        int         n;

        slt = str.split("-");
        n = slt.length;
        if (n != 2)
            Main.Error("Error reading map!\nCheck your links!");
        Node.addLink(slt);
        t = slt[0];
        slt[0] = slt[1];
        slt[1] = t;
        Node.addLink(slt);
    }

    public static void   storeLinks()
    {
        int         x;
        int         y;

        y = 0;
        while (Main.Map[y][0] != -1)
        {
            x = 0;
            while (Main.Map[y][x] != -1)
            {
                if (Main.Map[y][x] > 0 && Main.Map[y][x + 1] > 0)
                    readLinks(String.valueOf((x * 1000) + y) + "-"  + String.valueOf(((x + 1) * 1000) + y));
                if (Main.Map[y][x] > 0 && Main.Map[y + 1][x] > 0)
                    readLinks(String.valueOf((x * 1000) + y) + "-"  + String.valueOf((x * 1000) + (y + 1)));
                x++;
            }
            y++;
        }
    }

    public static void   storeNodes()
    {
        int         x;
        int         y;

        y = 0;
        while (Main.Map[y][0] != -1)
        {
            x = 0;
            while (Main.Map[y][x] != -1)
            {
                if (Main.Map[y][x] > 0)
                    readNewNode(String.valueOf((x * 1000) + y) + " " + x + " " + y);
                x++;
            }
            y++;
        }
    }

    public static void   readMap()
    {
        String      str;
        String      slt[];
        int         mapSize;
        int         x;
        int         y;
        int         i;

        mapSize = 0;
        y = 0;
        while ((str = Read.readLn()) != null)
        {
             System.out.println(str);
            slt = str.split(" ");
            if (mapSize == 0)
                mapSize = slt.length;
            else if (slt.length != mapSize)
                Main.Error("The map is'nt defined properly!");

            x = 0;
            i = 0;
            while (i < slt.length)
            {
                if ( slt[i].toLowerCase().contentEquals("w"))
                    Main.Map[y][x] = 0;
                if (slt[i].contentEquals("."))
                    Main.Map[y][x] = 1;
                if (slt[i].toLowerCase().contentEquals("s"))
                    Main.Map[y][x] = 2;
                if (slt[i].toLowerCase().contentEquals("e"))
                    Main.Map[y][x] = 3;
                Main.Map[y][x + 1] = -1;
                i++;
                x++;
            }
            y++;
            Main.Map[y][0] = -1;
        }
        storeNodes();
        storeLinks();
    }
}
